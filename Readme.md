The codes in this repository were used for the analysis behind the following paper:

1. Kumar A, Sherrington D, Wilson M, Thorpe MF: Ring statistics of silica bilayers. Journal of Physics: Condensed Matter 2014, 26:395401.

